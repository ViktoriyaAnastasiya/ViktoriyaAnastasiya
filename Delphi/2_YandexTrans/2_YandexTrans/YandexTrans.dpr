library YandexTrans;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  ShareMem,
  Windows, Messages, SysUtils, Classes, Variants, Graphics, Controls, Forms,
  Dialogs, IdTCPConnection, IdTCPClient, IdHTTP, IdBaseComponent, IdComponent,
  IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  StdCtrls,
  uLkJSON in 'uLkJSON.pas';//���� ������ ���� �� �������� ���������� (������� JSON-������)

{$R *.res}

//���� ������������ (API-����), ��� ������� � ������-�������
var UserKey:UnicodeString = '';

//���������� ���� ������������ (API-����)
function GetKey:UnicodeString; export; stdcall;
begin
  Result:=UserKey;
end;

//������ ���� ������������ (API-����)
procedure SetKey(aKey:UnicodeString);  export; stdcall;
begin
  UserKey:=aKey;
end;

//������ ������ � ������-�������
//���������� � Res ������ ��������� ������ ��� ��������
//� ������� <������������>-<����> (��������, "�������-ru" "����������-en")
//� ������ ������ (���� ����������, ����� �������) - ������� ���������� false, � Res - �������� ������.
function GetLangs(var Res:UnicodeString):boolean;  export; stdcall;
var
    IdSSLIOHandlerSocketOpenSSLx: TIdSSLIOHandlerSocketOpenSSL;
    IdHTTPx: TIdHTTP;
    fUrl:UnicodeString;
    aSource:TStringList;
    js,xs:TlkJSONobject;
    i:integer;
begin
 fUrl:='https://translate.yandex.net/api/v1.5/tr.json/getLangs?&ui=ru&key='+UserKey;
 IdSSLIOHandlerSocketOpenSSLx:=TIdSSLIOHandlerSocketOpenSSL.Create(nil);
 IdHTTPx:=TIdHTTP.Create(nil);
 IdHTTPx.IOHandler:=IdSSLIOHandlerSocketOpenSSLx;
 aSource:=TStringList.Create();
 try
  Res:=IdHTTPx.Post(fUrl,aSource);
 except
   on E:Exception do
   begin
    Res:=E.Message;
    Result:=false;
    aSource.Free;
    IdHTTPx.Free;
    IdSSLIOHandlerSocketOpenSSLx.Free;
    Exit;
   end;
 end;
 //aSource.Free;
 IdHTTPx.Free;
 IdSSLIOHandlerSocketOpenSSLx.Free;

 try
  js := TlkJSON.ParseText(Res) as TlkJSONobject;
  xs:= js.Field['langs'] as TlkJSONobject;
  aSource.Sorted:=true;
  for i:=0 to xs.Count - 1 do
   aSource.Add(xs.getString(i)+'-'+xs.NameOf[i]);
 except
   on E:Exception do
   begin
    Res:=E.Message;
    Result:=false;
    aSource.Free;
    js.Free;
    Exit;
   end;
 end;

 Res:=aSource.Text;
 Result:=true;

 aSource.Free;
 js.Free;
end;

//URL-����������� ������/������, � ������� UTF-8
function URLEncode(S:UnicodeString):UnicodeString;
var Res:string;
    i,Count:integer;
    Code:word;
    Arr: array of AnsiChar;
begin
 SetLength(Arr,Length(S)*4+1);
 Count:=UnicodeToUtf8(PAnsiChar(Arr),PWideChar(S),Length(S)*4+1);
 Res:='';
 for i := 0 to Count - 2 do
   begin
    Code:= Byte(Arr[i]);
    if Code=32 then Res:=Res+'+'
    else Res:=Res+'%'+IntToHex(Code,2);
   end;

 SetLength(Arr,0);

 Result:=Res;
end;

{"code":200,"lang":"en-ru","text":["������, ���! \n����������� ������ �������� Unicode � ��������� utf-8 ������."]}

//������ ������ � ������-������� ��� �������� ������
//Lang-������� ����(����), Text-�������� �����, � SourceLang-�������� ��� ������������ �������� ����(����),
//� Res ������������ ������� ������.
//� ������ ������ (���� ����������, ����� �������) - ������� ���������� false, � Res - �������� ������.
function LangTranslate(Lang:UnicodeString; Text:UnicodeString;var SourceLang:UnicodeString;var Res:UnicodeString):boolean;  export; stdcall;
var
    IdSSLIOHandlerSocketOpenSSLx: TIdSSLIOHandlerSocketOpenSSL;
    IdHTTPx: TIdHTTP;
    fUrl,s:UnicodeString;
    aSource:TStringList;
    js:TlkJSONobject;
    jl:TlkJSONlist;
    i:integer;
begin
 fUrl:='https://translate.yandex.net/api/v1.5/tr.json/translate?lang='+Lang+'&text='+URLEncode(Text)+'&key='+UserKey;
 IdSSLIOHandlerSocketOpenSSLx:=TIdSSLIOHandlerSocketOpenSSL.Create(nil);
 IdHTTPx:=TIdHTTP.Create(nil);
 IdHTTPx.IOHandler:=IdSSLIOHandlerSocketOpenSSLx;
 aSource:=TStringList.Create();
 try
  Res:=IdHTTPx.Post(fUrl,aSource);
 except
   on E:Exception do
   begin
    Res:=E.Message;
    Result:=false;
    aSource.Free;
    IdHTTPx.Free;
    IdSSLIOHandlerSocketOpenSSLx.Free;
    Exit;
   end;
 end;
 aSource.Free;
 IdHTTPx.Free;
 IdSSLIOHandlerSocketOpenSSLx.Free;

 try
  js := TlkJSON.ParseText(Res) as TlkJSONobject;
  jl:= js.Field['text'] as TlkJSONlist;
  s:=js.getString('lang');
  SourceLang:=Copy(s,1,Pos('-',S)-1);//��������� �������� ����
  s:=jl.getString(0);  //��������� �������
 except
   on E:Exception do
   begin
    Res:=E.Message;
    Result:=false;
    js.Free;
    Exit;
   end;
 end;

 //������-������ ���������� � �������� �������� ������ ���������� #32#10 (� ����� #13#10)
 for i:=1 to Length(s)-1 do
  if(s[i]=#32)and(s[i+1]=#10) then s[i]:=#13;

 Res:=s;
 Result:=true;

 js.Free;
end;

//�������������� ����������� �������
exports
  GetKey,
  SetKey,
  GetLangs,
  LangTranslate;

begin
end.

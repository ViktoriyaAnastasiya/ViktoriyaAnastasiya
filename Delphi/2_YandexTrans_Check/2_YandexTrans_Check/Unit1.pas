unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, ADODB;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    TranslateButton: TButton;
    EnterKeyButton: TButton;
    ADOConnection1: TADOConnection;
    ADOTable2: TADOTable;
    ADOTable2ID: TAutoIncField;
    ADOTable2Code: TWideStringField;
    ADOTable2Name: TWideStringField;
    Memo2: TMemo;
    SourceBox: TComboBox;
    DestBox: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    ADOQuery1: TADOQuery;
    ADOQuery1Source: TWideMemoField;
    ADOQuery1Dest: TWideMemoField;
    ADOQuery1SourceLang: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure TranslateButtonClick(Sender: TObject);
    procedure EnterKeyButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Langs:TStringList;  //������ �������� ������ (��� �����. �������)

    //����, ������������ - ��������� �� ���� ���� � ���� ������������.
    FInChain : Boolean;
    //����� ����, ������� � ���� ������������ ����� �� ����.
    FNextViewer : HWND;

    Key:UnicodeString;    //���� ������������ � ������� �������
    IsInputQuery:boolean; //������� �������� ���� ������� ����� ������������

    //���������� � ������, ���� ���������� ���� ������������.
    procedure WMChangeCBChain(var Msg: TWMChangeCBChain); message WM_CHANGECBCHAIN;
    //���������� � ������, ���� ���������� ������ ������ ����������.
    procedure WMDrawClipboard(var Msg: TWMDrawClipboard); message WM_DRAWCLIPBOARD;

    //������������������ � ���� ������������.
    procedure EnableView;
    //����� �� ���� ������������.
    procedure DisableView;

    //������ ������ ������, ��������� ��� ��������
    //�� ������-������� (����������� ������� Langs � ��), ��� �� ��, ���� ������ ����������
    function GetLangsFromServer(IsMess:boolean):boolean;

    //������ ��������:Lang-������� ����, Text-�����, SourceLang-�������� ����, Res-�������
    //������ ������ � ��. ��� ���������� ������ � �� - ������ � ������-������
    function Translate(Lang:UnicodeString; Text:UnicodeString;var SourceLang:UnicodeString;var Res:UnicodeString):boolean;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  Clipbrd, Registry;

(*   ������ ����������� � �� Access (� trans_cache.mdb � ����� � exe-������)
Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=E:\alex\TMP\Delphi2010\2_YandexTrans_Check\trans_cache.mdb;Mode=Share Deny None;Persist Security Info=False;Jet OLEDB:System database="";Jet OLEDB:Registry Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False;
 *)

//������� �� ����������� ���������� ������� � ������-������� (����������� ����������)
function GetKey:UnicodeString; far; stdcall; external 'YandexTrans.dll';
procedure SetKey(aKey:UnicodeString); far; stdcall; external 'YandexTrans.dll';
function GetLangs(var Res:UnicodeString):boolean; far; stdcall; external 'YandexTrans.dll';
function LangTranslate(Lang:UnicodeString; Text:UnicodeString;var SourceLang:UnicodeString;var Res:UnicodeString):boolean; far; stdcall; external 'YandexTrans.dll';



//���������� � ������, ���� �� ���� ��������� �����-�� �����������.
procedure TForm1.WMChangeCBChain(var Msg: TWMChangeCBChain);
begin
  //���� ������������ �� ������� ������: ����� ������ - �1 - �2 - �3 - �4 - �5 - �6
  //�1...�6 - ��� ����, ������������������ ��� �����������.
  //�����������, �� - ��� �3, ��� ��� ���������
  //�� ���� - ��� �4 - �� ���� ��������� FNextViewer
  //� ��� ��� ��������� �� �4 - ��� �5.
  //Msg.Remove - ����� (��������� ������������� Windows) ���������� ����
  //Msg.Next - ����� ���� ����, ������� ����������� �� ��������� �����

  //���� ���������, ��� �� ���� ������������ ��������� �4, �� �� ������ ����������
  //� ���� �5. � ���������� ���������: ����� ������ - �1 - �2 - �3 - �5 - �6
  if Msg.Remove = FNextViewer then
    FNextViewer := Msg.Next
  //���� �� ���� ������������ ��������� ����� �� ������ �4, �� �� ������ ������
  //��������� ��������� ���������� � ���� - �. �. �4.
  else
    SendMessage(FNextViewer, WM_CHANGECBCHAIN, Msg.Remove, Msg.Next);

  //����� ���� ����������� � ������ ���� - �. �. ����� �1. �������
  //������ ���� �� ���� ������������ �������� � �����������.
end;

//���������� � ������, ���� ���������� ������ ������ ����������.
//-��������� ����� �� ������ ������ � Memo1
//-��������� ������� ����������� ������� Translate
//-���������� ��������� � Memo2(�������) � � SourceBox(�������� ����)
procedure TForm1.WMDrawClipboard(var Msg: TWMDrawClipboard);
var
  i : Integer;
  Lang,Text:UnicodeString;
  SourceLang,Res:UnicodeString;
begin
  //��������� �� ���������� ���������� �� ���� �����������.
  if FNextViewer <> 0 then begin
    SendMessage(FNextViewer, WM_DRAWCLIPBOARD, 0, 0);
    Msg.Result:=0;
  end;

  //���� ���� ��������� ������ -
  if Clipboard.HasFormat(CF_TEXT) then
  begin
   //���� ���������� ���������, � ���� �� ��������, � ����� �� ����� ������ ����� - ��������� �������
   if not Application.Active and (WindowState<>wsMinimized) and not IsInputQuery  then
   begin
    Text:=Clipboard.AsText;
    Memo1.Text:=Text;
    Lang:=ADOTable2.Lookup('Name',DestBox.Items[DestBox.ItemIndex],'Code');
    if SourceBox.ItemIndex=0 then SourceLang:=''
    else SourceLang:=ADOTable2.Lookup('Name',SourceBox.Items[SourceBox.ItemIndex],'Code');
    if Translate(Lang,Text,SourceLang,Res) then
    begin
     SourceBox.ItemIndex:=SourceBox.Items.IndexOf(ADOTable2.Lookup('Code',SourceLang,'Name'));
     Memo2.Text:=Res;
    end;
   end;
  end;

end;

//����������� � ���� ������������.
procedure TForm1.EnableView;
begin
  //���� �� ��� ���������� � ���� - �������.
  if FInChain then begin
    Exit;
  end;
  FNextViewer := SetClipboardViewer(Handle);
  FInChain := True;
end;

//���������� ������ "������ ���� ������������..."
//���������� ������������ ������ ����
//���� ���� ����� - ����������� � ������� ������ ������, � ��������� ���� � ������ Windows
procedure TForm1.EnterKeyButtonClick(Sender: TObject);
var NewKey,RuText:UnicodeString;
    Req:TRegistry;
begin
  //
  NewKey:=Key;
  IsInputQuery:=true;
  FormStyle:=fsNormal;
  if InputQuery('','',NewKey) then
  begin
    Key:=NewKey;
    if GetLangsFromServer(true) then
    begin
      SourceBox.Items.Clear;
      SourceBox.Items.Add('-�����������-');
      SourceBox.Items.AddStrings(Langs);
      SourceBox.ItemIndex:=0;
      DestBox.Items.Clear;
      DestBox.Items.AddStrings(Langs);
      RuText:=ADOTable2.Lookup('Code','ru','Name');
      if RuText<>'' then  DestBox.ItemIndex:=Langs.IndexOf(RuText);

      Req:=TRegistry.Create;
      Req.RootKey:=HKEY_CURRENT_USER;
      Req.OpenKey('\Software\YandexTrans_Check',true);
      Req.WriteString('UserKey',Key);
      Req.Free;
    end;
  end;
  FormStyle:=fsStayOnTop;
  IsInputQuery:=false;
end;

//���������� �� ���� ������������ �� ������� ������.
procedure TForm1.DisableView;
begin
  //���� �� ��� ��������� �� ���� - �������.
  if not FInChain then Exit;

  ChangeClipboardChain(Handle, FNextViewer);
  FInChain := False;
end;


//������ ������ ������, ��������� ��� ��������
//�� ������-������� (����������� ������� Langs � ��), ��� �� ��, ���� ������ ����������
function TForm1.GetLangsFromServer(IsMess:boolean):boolean;
var Res:string;
    List:TStringList;
    L1,L2:UnicodeString;
    i,p:integer;
begin
 SetKey(Key);
 if GetLangs(Res) then
 begin
  Langs.Clear;
  List:=TStringList.Create;
  List.Text:=Res;
  for i:=0 to List.Count-1 do
  begin
    p:=Pos('-',List[i]);
    L1:=Copy(List[i],1,p-1);
    L2:=Copy(List[i],p+1,Length(List[i])-p);
    Langs.Add(L1);
    if not ADOTable2.Locate('Code;Name',VarArrayOf([L2,L1]),[]) then
    begin
     if ADOTable2.Locate('Code',L2,[]) then ADOTable2.Delete;
     if ADOTable2.Locate('Name',L1,[]) then ADOTable2.Delete;
     ADOTable2.Insert;
     ADOTable2Code.AsString:=L2;
     ADOTable2Name.AsString:=L1;
     ADOTable2.Post;
    end;

  end;

  ADOTable2.First;
  while not ADOTable2.Eof do
  begin
    if Langs.IndexOf(ADOTable2Name.AsString)<0 then ADOTable2.Delete
    else ADOTable2.Next;
  end;

  Result:=true;
 end
 else begin
       if IsMess then ShowMessage(Res)
       else begin
              Langs.Clear;
              ADOTable2.First;
              while not ADOTable2.Eof do
              begin
                Langs.Add(ADOTable2Name.AsString);
                ADOTable2.Next;
              end;
            end;
       Result:=false;
      end;
end;

//���������� ������ (������) � �������� � SQL-������ (������������ ��������� ����������)
function ToSQLLiteral(Source:UnicodeString):UnicodeString;
  var Res:UnicodeString;
      LastI,Count,i:integer;
begin
 LastI:=1;
 Count:=0;
 Res:='';
 for i:=1 to Length(Source) do
 begin
  inc(Count);
  if Source[i]='''' then
  begin
    Res:=Res+Copy(Source,LastI,Count)+'''';
    LastI:=i+1;
    Count:=0;
  end;
 end;
 if Count>0 then Res:=Res+Copy(Source,LastI,Count);
 Result:=Res;
end;

//������ ��������:Lang-������� ����, Text-�����, SourceLang-�������� ����, Res-�������
//������ ������ � ��. ��� ���������� ������ � �� - ������ � ������-������
function TForm1.Translate(Lang:UnicodeString; Text:UnicodeString;var SourceLang:UnicodeString;var Res:UnicodeString):boolean;
var s:UnicodeString;
    p:integer;
begin
 ADOQuery1.SQL.Clear;
 ADOQuery1.SQL.Add('select Source,Dest,SourceLang');
 ADOQuery1.SQL.Add('from Trans');
 if SourceLang='' then ADOQuery1.SQL.Add('where(DestLang='''+Lang+''')and(SourceLen='+IntToStr(Length(Text))+')')
 else ADOQuery1.SQL.Add('where(SourceLang='''+SourceLang+''')and(DestLang='''+Lang+''')and(SourceLen='+IntToStr(Length(Text))+')');

 ADOQuery1.Open;
 while not ADOQuery1.Eof do
 begin
  if ADOQuery1Source.AsString=Text then
  begin
    SourceLang:=ADOQuery1SourceLang.AsString;
    Res:=ADOQuery1Dest.AsString;
    Result:=true;
    ADOQuery1.Close;
    Exit;
  end;
  ADOQuery1.Next;
 end;
 ADOQuery1.Close;

 if Key='' then
 begin
       Memo2.Text:='������ ���������� ��������:'#13#10'����������� ���� ������������ ��� ������� � ������-�������';
       Result:=false;
       Exit;
 end;

 SetKey(Key);
 if SourceLang='' then s:=Lang
 else s:=SourceLang+'-'+Lang;
 if LangTranslate(s,Text,SourceLang,Res) then
 begin
  ADOQuery1.SQL.Clear;
  ADOQuery1.SQL.Add('insert into Trans (SourceLang, SourceLen, Source, DestLang, Dest)');
  s:='values ('''+SourceLang+''', '+IntToStr(Length(Text))+', '''+ToSQLLiteral(Text)+''', '''+
              Lang+''', '''+ToSQLLiteral(Res)+''')';
  ADOQuery1.SQL.Add(s);
  ADOQuery1.ExecSQL;

  Result:=true;
 end
 else begin
       Memo2.Text:='������ ���������� ��������:'#13#10+Res;
       Result:=false;
      end;
end;

//���������� ������ "��������� -->"
//��������� ������� ����������� ������� Translate
//���������� ��������� � Memo2(�������) � � SourceBox(�������� ����)
procedure TForm1.TranslateButtonClick(Sender: TObject);
var Lang,Text:UnicodeString;
    SourceLang,Res:UnicodeString;

begin
 Lang:=ADOTable2.Lookup('Name',DestBox.Items[DestBox.ItemIndex],'Code');
 if SourceBox.ItemIndex=0 then SourceLang:=''
 else SourceLang:=ADOTable2.Lookup('Name',SourceBox.Items[SourceBox.ItemIndex],'Code');
 Text:=Memo1.Text;
 if Translate(Lang,Text,SourceLang,Res) then
 begin
  SourceBox.ItemIndex:=SourceBox.Items.IndexOf(ADOTable2.Lookup('Code',SourceLang,'Name'));
  Memo2.Text:=Res;
 end;
end;

//���������� �������� ���� ��������� (������������ ��������)
procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 DisableView;
 Langs.Free;
 ADOTable2.Active:=false;
 ADOConnection1.Connected:=false;
end;

//���������� ��������� �������� ����
//������������ ������ � ��������� �����������
//(������������� - ������������ ��������)
procedure TForm1.FormResize(Sender: TObject);
var x:integer;
begin
 x:=ClientWidth div 2;
 Memo1.Left:=8;
 Memo1.Width:=x-12;
 SourceBox.Left:=x-4-SourceBox.Width;
 Memo2.Left:=x+4;
 Memo2.Width:=ClientWidth-x-12;
 Label2.Left:=x+4;
end;

//���������� �������� ����
//-��������� ���������� � ��;
//-������ �� ������� ���� ������������;
//-������ ������ � ������-������� ��� ��������� ������ ������;
//-�������������� ����������.
procedure TForm1.FormShow(Sender: TObject);
var RuText:UnicodeString;
    Req:TRegistry;
begin
 Langs:=TStringList.Create;
 Langs.Sorted:=true;

 Top:=0;
 Left:=Screen.Width-Width;

 ADOConnection1.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source='+
                                  ExtractFilePath(ParamStr(0))+'trans_cache.mdb'+
                                  ';Mode=Share Deny None;Persist Security Info=False;Jet OLEDB:System database="";'+
                                  'Jet OLEDB:Registry Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;'+
                                  'Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;'+
                                  'Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database Password="";'+
                                  'Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;'+
                                  'Jet OLEDB:Don''t Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False;';
 ADOConnection1.Connected:=true;
 ADOTable2.Active:=true;

 Key:='';
 Req:=TRegistry.Create;
 Req.RootKey:=HKEY_CURRENT_USER;
 if Req.KeyExists('\Software\YandexTrans_Check') then
  begin
   Req.OpenKey('\Software\YandexTrans_Check',false);
   if Req.ValueExists('UserKey') then Key:=Req.ReadString('UserKey');
  end;
  Req.Free;

 GetLangsFromServer(false);

 SourceBox.Items.Clear;
 SourceBox.Items.Add('-�����������-');
 SourceBox.Items.AddStrings(Langs);
 SourceBox.ItemIndex:=0;
 DestBox.Items.Clear;
 DestBox.Items.AddStrings(Langs);
 RuText:=ADOTable2.Lookup('Code','ru','Name');
 if RuText<>'' then  DestBox.ItemIndex:=Langs.IndexOf(RuText);

 IsInputQuery:=false;
 FInChain:=false;
 EnableView;
end;

end.

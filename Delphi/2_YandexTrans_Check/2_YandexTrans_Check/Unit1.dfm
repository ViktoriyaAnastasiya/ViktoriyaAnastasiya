object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1054#1085#1083#1072#1081#1085'-'#1087#1077#1088#1077#1074#1086#1076#1095#1080#1082' ('#1095#1077#1088#1077#1079' '#1071#1085#1076#1077#1082#1089', '#1089' '#1083#1086#1082#1072#1083#1100#1085#1099#1084' '#1082#1101#1096#1077#1084')'
  ClientHeight = 349
  ClientWidth = 698
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnResize = FormResize
  OnShow = FormShow
  DesignSize = (
    698
    349)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 61
    Width = 100
    Height = 13
    Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1090#1077#1082#1089#1090':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 351
    Top = 61
    Width = 55
    Height = 13
    Caption = #1055#1077#1088#1077#1074#1086#1076':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Memo1: TMemo
    Left = 8
    Top = 80
    Width = 337
    Height = 261
    Anchors = [akLeft, akTop, akBottom]
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object TranslateButton: TButton
    Left = 272
    Top = 8
    Width = 153
    Height = 25
    Anchors = [akTop]
    Caption = #1055#1077#1088#1077#1074#1077#1089#1090#1080' -->'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = TranslateButtonClick
  end
  object EnterKeyButton: TButton
    Left = 512
    Top = 8
    Width = 178
    Height = 25
    Anchors = [akTop, akRight]
    Caption = #1042#1074#1077#1089#1090#1080' '#1082#1083#1102#1095' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103'...'
    TabOrder = 2
    OnClick = EnterKeyButtonClick
  end
  object Memo2: TMemo
    Left = 353
    Top = 80
    Width = 337
    Height = 261
    Anchors = [akTop, akRight, akBottom]
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object SourceBox: TComboBox
    Left = 159
    Top = 53
    Width = 186
    Height = 21
    Style = csDropDownList
    TabOrder = 4
  end
  object DestBox: TComboBox
    Left = 503
    Top = 53
    Width = 187
    Height = 21
    Style = csDropDownList
    Anchors = [akTop, akRight]
    TabOrder = 5
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=E:\al' +
      'ex\TMP\Delphi2010\2_YandexTrans_Check\trans_cache.mdb;Mode=Share' +
      ' Deny None;Persist Security Info=False;Jet OLEDB:System database' +
      '="";Jet OLEDB:Registry Path="";Jet OLEDB:Database Password="";Je' +
      't OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLED' +
      'B:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1' +
      ';Jet OLEDB:New Database Password="";Jet OLEDB:Create System Data' +
      'base=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don'#39't Copy' +
      ' Locale on Compact=False;Jet OLEDB:Compact Without Replica Repai' +
      'r=False;Jet OLEDB:SFP=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 544
    Top = 264
  end
  object ADOTable2: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Langs'
    Left = 456
    Top = 264
    object ADOTable2ID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object ADOTable2Code: TWideStringField
      FieldName = 'Code'
      Size = 5
    end
    object ADOTable2Name: TWideStringField
      FieldName = 'Name'
      Size = 50
    end
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select Source,Dest,SourceLang'
      'from Trans'
      'where(SourceLang='#39'e'#39#39'n'#39')and(DestLang='#39'r'#39#39'u'#39')')
    Left = 384
    Top = 216
    object ADOQuery1Source: TWideMemoField
      FieldName = 'Source'
      BlobType = ftWideMemo
    end
    object ADOQuery1Dest: TWideMemoField
      FieldName = 'Dest'
      BlobType = ftWideMemo
    end
    object ADOQuery1SourceLang: TWideStringField
      FieldName = 'SourceLang'
      Size = 5
    end
  end
end
